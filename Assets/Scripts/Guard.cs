﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guard : Enemy
{
    Vector2 leftPos;
    Vector2 rightPos;
    Vector2 originalPos;
    Rigidbody2D body;

    public Direction direction;
    public float speed = 1f;
    public float rotateSpeed = 1;

    override protected void Start()
    {
        base.Start();
        body = GetComponent<Rigidbody2D>();
        originalPos = transform.position;

        int choice = System.Convert.ToInt32(Random.value);
        if(choice == 0)
        {
            choice = System.Convert.ToInt32(Random.value);
            direction = choice == 0 ? Direction.Left : Direction.Right;
        }
        else
        {
            choice = System.Convert.ToInt32(Random.value);
            direction = choice == 0 ? Direction.Up : Direction.Down;
        }

        switch (direction)
        {
            case Direction.Left:
                leftPos = new Vector2(transform.position.x - 5f, transform.position.y);
                rightPos = originalPos;
                break;
            case Direction.Right:
                rightPos = new Vector2(transform.position.x + 5f, transform.position.y);
                leftPos = originalPos;
                break;
            case Direction.Up:
                leftPos = new Vector2(transform.position.x, transform.position.y + 5f);
                rightPos = originalPos;
                break;
            case Direction.Down:
                rightPos = new Vector2(transform.position.x, transform.position.y - 5f);
                leftPos = originalPos;
                break;
        }
	}

    override protected void Update()
    {
        base.Update();
        var moveDirection = Vector2.zero;
        switch (direction)
        {
            case Direction.Left:
                moveDirection = (leftPos - (Vector2)transform.position).normalized;

                if (Vector3.Distance(leftPos, transform.position) < .1f)
                    direction = Direction.Right;
                break;
            case Direction.Right:
                moveDirection = (rightPos - (Vector2)transform.position).normalized;

                if (Vector3.Distance(rightPos, transform.position) < .1f)
                    direction = Direction.Left;
                break;
            case Direction.Up:
                moveDirection = (rightPos - (Vector2)transform.position).normalized;

                if (Vector3.Distance(rightPos, transform.position) < .1f)
                    direction = Direction.Down;
                break;
            case Direction.Down:
                moveDirection = (leftPos - (Vector2)transform.position).normalized;

                if (Vector3.Distance(leftPos, transform.position) < .1f)
                    direction = Direction.Up;
                break;
        }

        body.velocity = moveDirection * speed;

        var angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.AngleAxis(angle, Vector3.forward), rotateSpeed);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag != "Enemy" && collision.collider.tag != "Cheese")
        {
            switch (direction)
            {
                case Direction.Left:
                    direction = Direction.Right;
                    break;
                case Direction.Right:
                    direction = Direction.Left;
                    break;
                case Direction.Up:
                    direction = Direction.Down;
                    break;
                case Direction.Down:
                    direction = Direction.Up;
                    break;
            }
        }
    }

    public enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }
}
