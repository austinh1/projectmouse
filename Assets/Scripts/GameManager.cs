﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public MachineController players;
    public GameObject prefabShooter;
    public GameObject prefabGuard;
    public GameObject prefabFollower;
    public GameObject prefabCheese;
    public GameObject menu;
    public GameObject gameOver;
    public GameObject hud;
    public Text numberOfCheese;

    public float spawnMinDistance;
    public float spawnXMaxDistance;
    public float spawnYMaxDistance;
    public float spawnDistanceFromPlayer;
    public float spawnDistanceFromOther;
    public float maxEnemyCount;
    public int numberOfInitialCheeses;

    public GameState State { get; set; }

    float timer;
    float interval;
    List<GameObject> possibleEnemies;

    void Start ()
    {
        State = GameState.Menu;
        timer = interval;

        MakeCheese(numberOfInitialCheeses, 4);
    }

    void Update ()
    {
        switch(State)
        {
            case GameState.Menu:
                Menu();
                break;
            case GameState.InProgress:
                InProgress();
                break;
            case GameState.GameOver:
                GameOver();
                break;
        }
        
	}

    void Menu()
    {
        Time.timeScale = 0;
        menu.SetActive(true);
        gameOver.SetActive(false);
        hud.SetActive(false);

        var start = false;
        foreach(var player in ReInput.players.AllPlayers)
        {
            if(player.GetAnyButtonDown())
            {
                start = true;
            }
        }

        if(start)
        {
            State = GameState.InProgress;
        }
    }

    void InProgress()
    {
        Time.timeScale = 1;
        menu.SetActive(false);
        gameOver.SetActive(false);
        hud.SetActive(true);
        hud.transform.position = new Vector2(Screen.width * .9f, Screen.height * .9f);
        numberOfCheese.text = string.Format("{0}", players.Cheese.ToString());

        var difficulty = DetermineDifficulty(players.Cheese);

        switch (difficulty)
        {
            case Difficulty.VeryEasy:
                VeryEasy();
                break;
            case Difficulty.Easy:
                Easy();
                break;
            case Difficulty.Medium:
                Medium();
                break;
            case Difficulty.Hard:
                Hard();
                break;
            case Difficulty.VeryHard:
                VeryHard();
                break;
        }

        if (timer <= 0f)
        {
            if (transform.childCount <= maxEnemyCount)
                Spawn();

            timer = interval;
        }

        timer -= Time.deltaTime;
    }

    void GameOver()
    {
        Time.timeScale = 0;
        gameOver.SetActive(true);
        menu.SetActive(false);
        hud.SetActive(true);

        var restart = false;
        foreach (var player in ReInput.players.AllPlayers)
        {
            if (player.GetAnyButtonDown())
            {
                restart = true;
            }
        }

        if(restart)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void EnterGameOver()
    {
        State = GameState.GameOver;
    }

    void Paused()
    {
        Time.timeScale = 0;
    }

    Vector2 RandomPoint()
    {
        var acceptable = false;
        var newPosition = Vector2.zero;
        var tries = 1000;
        var attempts = 0;

        while (!acceptable && attempts < tries)
        {
            var x = RandomSign() * Random.Range(spawnMinDistance, spawnXMaxDistance);
            var y = RandomSign() * Random.Range(spawnMinDistance, spawnYMaxDistance);
            newPosition = new Vector2(x, y);

            if(Vector2.Distance(players.transform.position, newPosition) >= spawnDistanceFromPlayer)
            {
                var withinOtherDistance = false;
                foreach(Transform child in transform)
                {
                    if (Vector2.Distance(child.transform.position, newPosition) < spawnDistanceFromOther)
                    {
                        withinOtherDistance = true;
                    }
                }

                acceptable = !withinOtherDistance;
            }

            attempts++;
        }

        return newPosition;
    }

    void MakeCheese(int cheeseGroups, int maxAmountInGroup)
    {
        for (int i = 0; i < cheeseGroups; i++)
        {
            var position = RandomPoint();
            var numberOfCheeses = Random.Range(1, maxAmountInGroup);

            for (int j = 0; j < numberOfCheeses; j++)
            {
                var cheese = Instantiate(prefabCheese);
                var variance = 4f;
                cheese.transform.position = new Vector2(position.x + (RandomSign() * Random.value * variance), position.y + (RandomSign() * Random.value * variance));
                cheese.transform.eulerAngles = new Vector3(0, 0, Random.value * 360);
            }
        }
        
    }

    int RandomSign()
    {
        return Random.value < .5f ? 1 : -1;
    }

    Difficulty DetermineDifficulty(int numberOfPoints)
    {
        var difficulty = Difficulty.VeryEasy;

        if (numberOfPoints < 5)
            difficulty = Difficulty.VeryEasy;
        else if (numberOfPoints < 15)
            difficulty = Difficulty.Easy;
        else if (numberOfPoints < 35)
            difficulty = Difficulty.Medium;
        else if (numberOfPoints < 50)
            difficulty = Difficulty.Hard;
        else difficulty = Difficulty.VeryHard;

        return difficulty;
    }

    void Spawn()
    {
        var enemy = Instantiate(RandomEnemy(), transform);
        enemy.transform.position = RandomPoint();

        MakeCheese(Random.Range(1, 3), Random.Range(2, 4));
    }

    GameObject RandomEnemy()
    {
        return possibleEnemies[Random.Range(0, possibleEnemies.Count - 1)];
    }

    void VeryEasy()
    {
        possibleEnemies = new List<GameObject>()
        {
            prefabGuard,
            prefabGuard,
            prefabShooter
        };

        interval = 15f;
    }

    void Easy()
    {
        possibleEnemies = new List<GameObject>()
        {
            prefabGuard,
            prefabShooter,
            prefabShooter,
        };

        interval = 10f;
    }

    void Medium()
    {
        possibleEnemies = new List<GameObject>()
        {
            prefabGuard,
            prefabShooter,
            prefabFollower
        };

        interval = 8f;
    }

    void Hard()
    {
        possibleEnemies = new List<GameObject>()
        {
            prefabShooter,
            prefabShooter,
            prefabFollower
        };

        interval = 5f;
    }

    void VeryHard()
    {
        possibleEnemies = new List<GameObject>()
        {
            prefabShooter,
            prefabFollower,
            prefabFollower
        };

        interval = 3f;
    }

    public enum GameState
    {
        Menu,
        InProgress,
        GameOver
    }

    public enum Difficulty
    {
        VeryEasy,
        Easy,
        Medium,
        Hard,
        VeryHard
    }
}
