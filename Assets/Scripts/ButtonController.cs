﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    public bool active { get; set; }
    SpriteRenderer sprite;

    public Color activeColor;
    public Color idleColor;

	void Start ()
    {
        sprite = GetComponent<SpriteRenderer>();
        active = false;
	}
	
	void Update ()
    {
        sprite.color = active ? activeColor : idleColor;
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            active = true;
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            active = true;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            active = false;
        }
    }
}
