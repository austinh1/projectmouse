﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadMovement : MonoBehaviour
{
    public float speed;
    public GameObject bork;
    public float borkSpeed;
    public MachineController machine;

    HeadState state;
    Vector2 originalPosition;
    bool borked = false;

	void Start ()
    {
        originalPosition = transform.localPosition;
        state = HeadState.Idle;
        machine = transform.parent.parent.GetComponent<MachineController>();
	}
	
	void Update ()
    {
        Vector2 lerpToPosition = originalPosition;
		switch(state)
        {
            case HeadState.In:
                lerpToPosition = new Vector2(originalPosition.x, originalPosition.y - .4f);
                borked = false;
                if (Vector2.Distance(transform.localPosition, lerpToPosition) < .1f)
                    state = HeadState.Out;
                break;
            case HeadState.Out:
                lerpToPosition = new Vector2(originalPosition.x, originalPosition.y + .25f);

                if(!borked)
                {
                    Bork();
                    borked = true;
                }

                if (Vector2.Distance(transform.localPosition, lerpToPosition) < .1f)
                    state = HeadState.In;
                break;
            case HeadState.Idle:
                lerpToPosition = originalPosition;
                break;
        }

        transform.localPosition = Vector2.Lerp(transform.localPosition, lerpToPosition, Time.deltaTime * speed);
	}

    void Bork()
    {
        var b = Instantiate(bork);
        b.transform.position = new Vector2(transform.position.x, transform.position.y);
        b.transform.rotation = transform.rotation;
        b.GetComponent<BorkBeam>().speed = borkSpeed;
    }

    public void StartMovement()
    {
        if(state == HeadState.Idle)
            state = HeadState.Out;
    }

    public void StopMovement()
    {
        if(state != HeadState.Idle)
        state = HeadState.Idle;
    }

    public enum HeadState
    {
        Out,
        In,
        Idle
    }
}
