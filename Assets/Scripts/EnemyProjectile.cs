﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public float speed = 1f;

    void Start()
    {
    }

    void Update()
    {
        transform.Translate((Vector3.up / 100f) * speed);

        if (Vector2.Distance(transform.position, Vector2.zero) > 30)
            Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player Attack")
        {
            Destroy(gameObject);
        }
    }
}
