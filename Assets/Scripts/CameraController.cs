﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float followSpeed;
    public GameObject target;
    private MachineController targetMachineController;
    private Vector2 targetVector;
    private float xSpeed;
    private float ySpeed;

    void Start()
    {

    }
    void Update ()
    {
        targetMachineController = target.GetComponent<MachineController>();
        targetVector = targetMachineController.spinner.transform.up * targetMachineController.Speed * 1.5f;
        xSpeed = targetVector.x;
        ySpeed = targetVector.y;
        //(spinner.transform.up / 100f) * speed)

        var lerpToPositionCenter = new Vector3(target.transform.position.x, target.transform.position.y, -10f);
        var lerpToPosition = new Vector3(target.transform.position.x + xSpeed, target.transform.position.y + ySpeed, -10f);


        if (targetMachineController.CurrentHealth > 0)
        {
            transform.position = Vector3.Lerp(transform.position, lerpToPosition, Time.deltaTime * followSpeed);
        }else
        {
            transform.position = Vector3.Lerp(transform.position, lerpToPositionCenter, Time.deltaTime * followSpeed/2);
        }
    }
}
