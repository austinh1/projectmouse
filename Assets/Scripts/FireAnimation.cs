﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireAnimation : MonoBehaviour {
    private int count = 0;
    private int coolDown;

	// Use this for initialization
	void Start () {
		coolDown = Random.Range(10,30);
	}
	
	// Update is called once per frame
	void Update () {
        count++;

		if (count > coolDown)
        {
            count = 0;
            if (this.GetComponent<SpriteRenderer>().flipX == true)
            {
                this.GetComponent<SpriteRenderer>().flipX = false;
            }
            else
            {
                this.GetComponent<SpriteRenderer>().flipX = true;
            }
            coolDown = Random.Range(10,30);
        }
	}
}
