﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorkBeam : MonoBehaviour
{
    public float speed = 1f;
	void Start ()
    {
    }
	
	void Update ()
    {
        transform.Translate((Vector2.up / 100f) * speed);

        if (Vector2.Distance(transform.position, Vector2.zero) > 30)
            Destroy(gameObject);
	}
}
