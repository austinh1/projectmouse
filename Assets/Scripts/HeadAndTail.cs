﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadAndTail : MonoBehaviour
{
    [SerializeField]
    private GameObject spinner;

    MachineController machine;

	void Start ()
    {
        machine = transform.parent.GetComponent<MachineController>();
	}
	
	void Update ()
    {
        this.transform.rotation = spinner.transform.rotation;
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Cheese")
        {
            collision.GetComponent<Cheese>().Retrieve();
            machine.CollectCheese();
        }
    }
}
