﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : Enemy
{
    public float shootInterval;
    public float shootDistance;
    public float shootSpeed;
    public GameObject projectile;

    MachineController players;
    float shootTimer = 0f;

    override protected void Start()
    {
        base.Start();
        shootTimer = Random.value * shootInterval;
        players = GameObject.Find("Machine Dog").GetComponent<MachineController>();
	}
    
	override protected void Update ()
    {
        base.Update();

        if (Vector2.Distance(transform.position, players.transform.position) < shootDistance)
        {
            transform.up = players.transform.position - transform.position;
            
            if (shootTimer <= 0)
            {
                Shoot();
                shootTimer = shootInterval;
            }
        }

        shootTimer -= Time.deltaTime;
	}

    void Shoot()
    {
        var p = Instantiate(projectile);
        p.transform.position = transform.position;
        p.transform.rotation = transform.rotation;
        p.GetComponent<EnemyProjectile>().speed = shootSpeed;
    }
}
