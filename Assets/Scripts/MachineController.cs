﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineController : MonoBehaviour
{
    public Transform spinner;
    public ButtonController up;
    public ButtonController tailWhip;
    public ButtonController bork;
    public ButtonController shieldToggle;
    public TailMovement tailMovement;
    public HeadMovement headMovement;
    public GameObject fire;
    public SpriteRenderer emergencyLights;
    public GameObject shield;

    public float flashSpeed;
    public float maxSpeed;
    public float acceleration;
    public int maxHealth;

    public float Speed { get; set; }
    float lerpToSpeed = 0;
    public int CurrentHealth { get; set; }
    public int Cheese { get; set; }

    GameManager gameManager;

    void Start ()
    {
        CurrentHealth = maxHealth;
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
    }
	
	void Update ()
    {
        Move();
        if(gameManager.State == GameManager.GameState.InProgress)
        {
            if (CurrentHealth > 0)
            {
                emergencyLights.color = new Color(emergencyLights.color.r, emergencyLights.color.g, emergencyLights.color.b, 0f);
                TailWhip();
                Bork();
                Shield();
            }
            else if (CurrentHealth <= 0)
            {
                var alpha = Mathf.PingPong(Time.time * flashSpeed, .3f) + .1f;
                emergencyLights.color = new Color(emergencyLights.color.r, emergencyLights.color.g, emergencyLights.color.b, alpha);

                StopActions();

                if (CurrentHealth < 0)
                    gameManager.EnterGameOver();
            }
        }
        else
        {
            StopActions();
        }
    }

    void StopActions()
    {
        shield.SetActive(false);
        tailMovement.StopMovement();
        headMovement.StopMovement();
    }

    void Move()
    {
        var sign = 0;
        if (up.active)
            sign = 1;

        var accel = acceleration;
        if (sign != 0 && CurrentHealth > 0)
        {
            lerpToSpeed = maxSpeed * sign;
        }
        else
        {
            lerpToSpeed = 0;
            accel = acceleration * 2f;
        }

        Speed = Mathf.Lerp(Speed, lerpToSpeed, Time.deltaTime * accel);
        transform.Translate((spinner.transform.up / 100f) * Speed);
    }

    void TailWhip()
    {
        if (tailWhip.active)
        {
            tailMovement.StartMovement();
        }
        else if (tailMovement.Moving)
        {
            tailMovement.StopMovement();
        }
    }

    void Bork()
    {
        if (bork.active)
        {
            headMovement.StartMovement();
        }
        else
        {
            headMovement.StopMovement();
        }
    }

    public void CollectCheese()
    {
        Cheese += 1;
    }

    public void Damage(Vector2 collisionPosition)
    {
        var quadrant = CollisionQuadrant(collisionPosition);

        CurrentHealth -= 1;
        var f = Instantiate(fire, transform);
        var x = Random.Range(0.5f, 1f);
        var y = Random.Range(0.5f, 1f);
        f.transform.localPosition = new Vector2(x * quadrant.x, y * quadrant.y);
    }

    Vector2 CollisionQuadrant(Vector2 position)
    {
        var quadrant = Vector2.zero;
        if (position.x < transform.position.x && position.y < transform.position.y)
        {
            quadrant = new Vector2(-1, -1);
        }
        else if (position.x > transform.position.x && position.y < transform.position.y)
        {
            quadrant = new Vector2(1, -1);
        }
        else if (position.x > transform.position.x && position.y > transform.position.y)
        {
            quadrant = new Vector2(1, 1);
        }
        else if (position.x < transform.position.x && position.y > transform.position.y)
        {
            quadrant = new Vector2(-1, 1);
        }

        return quadrant;
    }

    public void Heal()
    {
        CurrentHealth += 1;
    }

    public void Bounce(float magnitude)
    {
        Speed = -Speed * magnitude;
    }


    void Shield()
    {
        shield.SetActive(shieldToggle.active);
    }
}
