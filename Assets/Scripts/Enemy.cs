﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float maxHealth = 1;

    float currentHealth; 

	protected virtual void Start ()
    {
        currentHealth = maxHealth;
	}
	
	protected virtual void Update ()
    {
		if(currentHealth <= 0f)
        {
            Destroy(gameObject);
        }
	}

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player Attack")
        {
            currentHealth -= 1f;
            Destroy(collision.gameObject);
        }
    }
}
