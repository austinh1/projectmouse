﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    private int count = 0;
    private int coolDown;

    void Start()
    {
        coolDown = Random.Range(5, 15);
    }

    void Update()
    {
        count++;

        if (count > coolDown)
        {
            count = 0;
            if (Random.Range(0,2) == 0)
            {
                this.GetComponent<SpriteRenderer>().flipX = !this.GetComponent<SpriteRenderer>().flipX;
            }
            else
            {
                this.GetComponent<SpriteRenderer>().flipY = !this.GetComponent<SpriteRenderer>().flipY;
            }
            coolDown = Random.Range(5, 15);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy Attack")
        {
            Destroy(collision.gameObject);
        }
    }
}
