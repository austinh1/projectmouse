﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
    public int playerId;
    public float speed;
    public float rotateSpeed;

    Player player;
    Rigidbody2D body;
    MachineController machine;
    float horizontalAxis;
    float verticalAxis;

    void Start ()
    {
        player = ReInput.players.GetPlayer(playerId);
        body = GetComponent<Rigidbody2D>();
        machine = transform.parent.GetComponent<MachineController>();
	}
	
	void Update ()
    {
        GetInput();

        Move();
	}

    void Move()
    {
        if (horizontalAxis != 0f || verticalAxis != 0f)
        {
            var vel = new Vector2(horizontalAxis * speed, verticalAxis * speed);
            body.velocity = new Vector2(horizontalAxis * speed, verticalAxis * speed);

            var moveDirection = body.velocity;
            var angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.AngleAxis(angle, Vector3.forward), rotateSpeed);
        }

        body.drag = 0f;
        if (horizontalAxis == 0f && verticalAxis == 0f)
            body.drag = 5f;

        if (body.velocity.magnitude >= speed)
        {
            body.velocity = body.velocity.normalized * speed;
        }
    }

    void GetInput()
    {
        horizontalAxis = player.GetAxis("Move Horizontal");
        verticalAxis = player.GetAxis("Move Vertical");
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Fire")
        {
            machine.Heal();
            Destroy(collision.gameObject);
        }
    }
}
