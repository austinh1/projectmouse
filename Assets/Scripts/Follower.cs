﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : Enemy
{
    public float speed;
    public float distance;
    public float rotateSpeed;
    Transform players;
    Rigidbody2D body;

    override protected void Start()
    {
        base.Start();
        players = GameObject.Find("Machine Dog").transform;
        body = GetComponent<Rigidbody2D>();
	}

    override protected void Update()
    {
        base.Update();
        if (Vector2.Distance(transform.position, players.position) < distance)
        {
            var moveDirection = (players.position - transform.position).normalized;
            body.velocity = moveDirection * speed;

            var angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.AngleAxis(angle, Vector3.forward), rotateSpeed);
        }
        else
        {
            body.velocity = Vector2.zero;
        }
	}
}
