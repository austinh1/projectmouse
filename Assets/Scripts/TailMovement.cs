﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TailMovement : MonoBehaviour
{
    int count = 0;
    TailState state = TailState.Center;
    public bool Moving { get { return state != TailState.Center; } }
    Collider2D playerAttackCollider;

    float currentRotation = 0;

    void Start ()
    {
        playerAttackCollider = GetComponent<Collider2D>();
    }

    void Update ()
    {
        count++;

        float ourAngle = transform.localEulerAngles.z;
        ourAngle = (ourAngle > 180) ? ourAngle - 360 : ourAngle;
        float targetAngle = 45;

        switch (state)
        {
            case TailState.Center:
                this.transform.localRotation = Quaternion.Lerp(this.transform.localRotation, new Quaternion(0, 0, Mathf.Deg2Rad * 0, 1), Time.deltaTime * 2f);
                break;
            case TailState.Left:
                this.transform.localRotation = Quaternion.Lerp(this.transform.localRotation, new Quaternion(0, 0, Mathf.Deg2Rad * -targetAngle, 1), Time.deltaTime * 2f);
                if (ourAngle < -targetAngle)
                {
                    state = TailState.Right;
                }
                break;
            case TailState.Right:
                this.transform.localRotation = Quaternion.Lerp(this.transform.localRotation, new Quaternion(0, 0, Mathf.Deg2Rad * targetAngle, 1), Time.deltaTime * 2f);
                if(ourAngle > targetAngle)
                {
                    state = TailState.Left;
                }
                break;
        }

        playerAttackCollider.enabled = state != TailState.Center;
    }

    public void StartMovement()
    {
        if(state == TailState.Center)
            state = TailState.Left;
    }

    public void StopMovement()
    {
        if(state != TailState.Center)
            state = TailState.Center;
    }

    public enum TailState
    {
        Left,
        Right,
        Center
    }
}
