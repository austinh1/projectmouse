﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyController : MonoBehaviour
{
    MachineController machine;
	void Start ()
    {
        machine = transform.parent.GetComponent<MachineController>();
	}
	
	void Update ()
    {
        transform.localPosition = Vector2.zero;
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "Wall")
        {
            machine.Bounce(2f);
        }
        else if (collision.collider.tag.Contains("Enemy"))
        {
            machine.Damage(collision.transform.position);
            Destroy(collision.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Contains("Enemy"))
        {
            machine.Damage(collision.transform.position);
            Destroy(collision.gameObject);
        }
    }
}
